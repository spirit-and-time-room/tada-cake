addForm = document.getElementsByClassName('item-add');
[].forEach.call(addForm,function(i){
    i.addEventListener('click', formCreate, false);
});

// フォーム生成
function formCreate(e) {
    // 追加フォームの2重表示防止のため
    if(document.getElementById('item_form')) {
        removeItem = document.getElementById('item_form');
        removeItemParent = removeItem.parentNode;
        removeItemParent.removeChild(removeItem);
    }
    itemType = e.target.dataset.type;
    form = document.createElement('form');
    form.id = 'item_form';
    form.method = 'post';

    itemLink = document.createElement("input");

    switch(itemType) {
        case 'heading':// 見出し
            itemData = document.createElement("input");
            itemData.type = 'text';
            itemData.name = itemType;
            itemData.placeholder = '見出しを入力';
            itemData.className = 'form-control';
            form.appendChild(itemData);

            addItem = document.createElement("h2");

            break;
        case 'text':// テキスト
            itemData = document.createElement("textarea");
            itemData.name = itemType;
            itemData.placeholder = 'テキストを入力';
            itemData.className = 'form-control';
            form.appendChild(itemData);

            addItem = document.createElement("p");

            break;
        case 'link':// リンク
            itemData = document.createElement("input");
            itemData.type = 'text';
            itemData.name = itemType;
            itemData.placeholder = 'リンクテキストを入力';
            itemData.className = 'form-control';
            form.appendChild(itemData);

            itemLink.type = 'text';
            itemLink.name = itemType;
            itemLink.placeholder = '追加するURLを入力';
            itemLink.className = 'form-control';
            form.appendChild(itemLink);

            addItem = document.createElement("a");

            break;
        // case 'image':// 画像
        //   break;
        // case 'movie':// 動画
        //   break;
    }

    // 保存
    var submit = document.createElement("button");
    submit.textContent = '保存';
    submit.id = 'item_save';
    submit.type = 'button';
    submit.className = 'btn btn-primary';
    submit.onclick = function() {
        // データ保存
        var r = new XMLHttpRequest();
        r.open("POST", '/items', true);
        r.setRequestHeader("X-Requested-With","XMLHttpRequest");
        r.setRequestHeader('Content-Type', 'application/json')
        r.send(JSON.stringify({
            type : itemType,
            link : itemLink.value,
            data : itemData.value,
            item_order : 1
        }));
        r.onreadystatechange = function () {
        if (r.readyState != 4 || r.status != 200) return;
            // var msg = JSON.parse(r.responseText);
            // var div = document.getElementById('message');
            // div.appendChild(document.createTextNode(msg["message"]));
        };
        //r.send("");

        itemCount = document.getElementsByClassName('post_list_item');
        itemCount = itemCount.length;

        var target = document.getElementById('post_target');
        var postItem = document.createElement("div");
        postItem.className = 'post_list_item';
        postItem.appendChild(addItem);

        itemId = document.createElement("input");
        itemId.type = 'hidden';
        itemId.name = 'item_id';
        itemId.value = itemCount+1;
        postItem.appendChild(itemId);

        itemOrder = document.createElement("input");
        itemOrder.type = 'hidden';
        itemOrder.name = 'item_order';
        itemOrder.value = 1;
        postItem.appendChild(itemOrder);

        addItem.innerHTML = itemData.value;
        target.insertBefore(postItem, target.firstChild);

        // 追加フォームの削除
        removeItemParent = form.parentNode;
        removeItemParent.removeChild(form);
    }
    form.appendChild(submit);

    // キャンセル
    var cancel = document.createElement("button");
    cancel.textContent = 'キャンセル';
    cancel.type = 'button';
    cancel.className = 'btn btn-default';
    cancel.onclick = function() {
        // 追加フォームの削除
        removeItemParent = form.parentNode;
        removeItemParent.removeChild(form);
    }
    form.appendChild(cancel);

    var target = document.getElementById('item_target');
    var targetPosition = document.getElementById('item_target_position');

    target.insertBefore(form, targetPosition.nextSibling);
}
