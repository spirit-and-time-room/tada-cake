<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class ItemsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function add()
    {
        // Ajaxリクエストの場合
        if ($this->request->is("ajax")) {
            $conn = ConnectionManager::get('default');
            $conn->execute('set @incr = 1; UPDATE items SET item_order = @incr := @incr+1;');

            // 入力されたデータの登録
            $items = TableRegistry::get('Items');
            $item = $items->newEntity($this->request->data, [
                'associated' => ['Items']
            ]);
            $items->save($item);
        } else {
            var_dump($items);
        }
    }
}
