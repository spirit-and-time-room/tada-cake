<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;

class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function view($post_id = null)
    {
        $posts = TableRegistry::get('Posts');
        $results = $posts
            ->find()
            ->where(['id' => $post_id])
            ->contain([
                'Tags',
                'Items' => function ($q) {
                    return $q->order(['item_order' => 'ASC']);
                }
            ])
            ->first()
            ->toArray();

        switch (array_key_exists('type', $_GET) ? $_GET['type'] : '') {
            case 'edit':
                $type = 'edit';
                break;
            case 'delete':
                $type = 'delete';
                break;
            default:
                $type = 'select';
                break;
        }
        $this->set('post_id', $post_id);
        $this->set('type', $type);
        $this->set('results', $results);
    }
}
