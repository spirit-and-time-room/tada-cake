<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Bootstrap Test</a>
        </div>
    </div>
</nav>
<div class="container theme-showcase" role="main">
    <div class="item">
        <div id="item_target">
            <?php if ($type == "edit") : ?>
            <ul id="item_target_position" class="item_list">
                <li><a href="javascript:void(0);" class="item-add" data-type="heading">heading（見出し）</a></li>
                <li><a href="javascript:void(0);" class="item-add" data-type="text">text（テキスト）</a></li>
                <li><a href="javascript:void(0);" class="item-add" data-type="link">link（リンク）</a></li>
                <!-- <li><a href="javascript:void(0);" class="item-add" data-type="image">image（画像）</a></li>
                <li><a href="javascript:void(0);" class="item-add" data-type="movie">movie（動画）</a></li> -->
            </ul>
            <?php else : ?>
            <a href="?type=edit">アイテムを追加</a>
            <?php endif; ?>
        </div>
    </div>
    <div class="post">
        <div id="post_target" class="post_list">
            <?php foreach ($results['items'] as $key => $value) : ?>
            <div class="post_list_item">
                <?php if ($value['type'] == 'heading') : ?>
                <h2><?php echo $value['data']; ?></h2>
                <?php elseif ($value['type'] == 'text') : ?>
                <p><?php echo $value['data']; ?></p>
                <?php elseif ($value['type'] == 'link') : ?>
                <a href="<?php echo $value['link']; ?>"><?php echo $value['data']; ?></a>
                <?php endif; ?>
                <input type="hidden" name="item_id" value="<?php echo $value['id']; ?>">
                <input type="hidden" name="item_order" value="<?php echo $value['item_order']; ?>">
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php echo $this->Html->script('test.js'); ?>
