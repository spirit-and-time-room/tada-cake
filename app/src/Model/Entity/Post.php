<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property string $id
 * @property string $item_id
 * @property string $tag_id
 * @property \Cake\I18n\Time $create_datetime
 * @property \Cake\I18n\Time $update_datetime
 *
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\Tag $tag
 */
class Post extends Entity
{

}
