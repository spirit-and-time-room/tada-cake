<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $item_id
 * @property string $type
 * @property string $link
 * @property string $data
 * @property int $item_order
 * @property \Cake\I18n\Time $create_datetime
 * @property \Cake\I18n\Time $update_datetime
 *
 * @property \App\Model\Entity\Item $item
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'item_id' => false
    ];
}
