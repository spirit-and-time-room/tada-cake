<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostItemsTable Test Case
 */
class PostItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostItemsTable
     */
    public $PostItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.post_items',
        'app.posts',
        'app.items',
        'app.tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PostItems') ? [] : ['className' => 'App\Model\Table\PostItemsTable'];
        $this->PostItems = TableRegistry::get('PostItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PostItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
