CREATE TABLE posts (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  description VARCHAR(255),
  create_datetime DATETIME,
  update_datetime DATETIME,
  PRIMARY KEY (id)
);
CREATE INDEX posts_id_key ON posts (id) USING btree;

CREATE TABLE post_items (
  id INTEGER NOT NULL AUTO_INCREMENT,
  post_id INTEGER NOT NULL,
  item_id INTEGER NOT NULL,
  create_datetime DATETIME,
  update_datetime DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE items (
  id INTEGER NOT NULL AUTO_INCREMENT,
  type TEXT COMMENT 'title,read,heading,text,link,image,movie',
  link VARCHAR(255),
  data VARCHAR(255),
  item_order INTEGER NOT NULL,
  create_datetime DATETIME,
  update_datetime DATETIME,
  PRIMARY KEY (id)
);
CREATE INDEX items_id_key ON items (id) USING btree;

CREATE TABLE post_tags (
  id INTEGER NOT NULL AUTO_INCREMENT,
  post_id INTEGER NOT NULL,
  tag_id INTEGER NOT NULL,
  create_datetime DATETIME,
  update_datetime DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE tags (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name TEXT,
  create_datetime DATETIME,
  update_datetime DATETIME,
  PRIMARY KEY (id)
);
CREATE INDEX tags_id_key ON tags (id) USING btree;
